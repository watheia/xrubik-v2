FROM node:lts-alpine as builder

ADD home /wa

# RUN yarn && \
#   bit init --harmony && \
#   bit import && \
#   bit install

WORKDIR /wa
RUN yarn global add preact-cli
RUN yarn && yarn preact build \
  --prerender \
  --preload \
  --inline-css \
  --sw \
  --esm

FROM node:lts-alpine

COPY --from=builder /wa/build /var/www/html
RUN npm install --global serve

EXPOSE 5000
CMD ["serve", "/var/www/html"]