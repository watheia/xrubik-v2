import React from "react"
import Logo from "./Logo"

export const Primary = (): JSX.Element => <Logo themeMode="light" />

export const Secondary = (): JSX.Element => <Logo themeMode="dark" />
