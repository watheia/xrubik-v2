import React from "react"
import { render } from "@testing-library/react"
import Copyright from "."

describe("ui/atoms/Copyright", () => {
  it("should render correctly", () => {
    render(<Copyright url="http://staging.watheia.org" label="Watheia" />)
  })
})
