import { defaultProps, Copyright, CopyrightProps } from "./Copyright"

export { defaultProps, Copyright }
export type { CopyrightProps }
export default Copyright
