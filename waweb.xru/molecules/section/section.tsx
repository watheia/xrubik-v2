import React from 'react';

export interface SectionProps extends React.HTMLAttributes<HTMLDivElement> {

};

export const Section = ( {children, ...rest}: SectionProps ) => {
  return (
    <div {...rest}>
      {children}
    </div>
  )
};