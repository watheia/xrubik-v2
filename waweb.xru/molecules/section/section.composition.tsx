import React from 'react';
import { Section } from './section';

// sets the Component preview in gallery view
export const BasicSection = () => {
  return <Section>hello from Section</Section>;
};
