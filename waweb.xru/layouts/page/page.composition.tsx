import React from 'react';
import { Page } from './page';

// sets the Component preview in gallery view
export const BasicPage = () => {
  return <Page>hello from Page</Page>;
};
