import React from 'react';

export interface PageProps extends React.HTMLAttributes<HTMLDivElement> {

};

export const Page = ( {children, ...rest}: PageProps ) => {
  return (
    <div {...rest}>
      {children}
    </div>
  )
};