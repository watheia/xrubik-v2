# NGINX Server Sample Application

## Building

```bash
pack build watheialabs/xrubik-v1 --builder paketobuildpacks/builder:base --buildpack gcr.io/paketo-buildpacks/nginx
```

## Running

```bash
docker run --tty --env PORT=5000 --publish 5000:5000 watheialabs/xrubik-v1
```
